(function() {
    'use strict';
    window.Main = {
        // ==== Local attributes ====
        MONTHS: ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"],
        DAYS: ["dimanche", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "lundi"],
        dateTime: null,


        // ===== API =====

        /**
         * Draw the date into a given DOM node
         * @param node - Where to render the time
         */
        showDate: function (node) {
            var dateTime = new Date();
            node.innerHTML = Main.DAYS[dateTime.getDay()] +
                " " + Main._pad(dateTime.getDate()) +
                " " + Main.MONTHS[dateTime.getMonth()] +
                " " + dateTime.getFullYear();
        },

        animateHeader: function () {
            document.getElementById("my-info").className = "opened";
            document.querySelector("header button").className = "opened";
        },


        // ===== Private functions =====

        /**
         * Add padding to a number (add '0' characters)
         * @param {int|string} number - The number to pad
         * @param {int} [width=2] - The width of the expected padded number
         * @returns {string}
         * @private
         */
        _pad: function (number, width) {
            width = (typeof width !== 'undefined') ? width : 2; // Default value
            number = number + ''; // Convert to string if it's a number
            return new Array(width - number.length + 1).join('0') + number;
        }
    };
}());
